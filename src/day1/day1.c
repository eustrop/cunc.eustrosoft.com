#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>
#define STR_SIZE 1024
char* NL_SEQ="\n"; // \n for unix, \r\n for DOS/Windows
char* sz_ident="CUNC day1.c"; /* comment */
char sz_copy[]="(c) EustroSoft.org 2020";
#define SL_LIC "LICENSE: BALES || MIT ||BSD on your choice"
char* sz_lic= SL_LIC ; char sz_buf[STR_SIZE];

int ccnt_for(char b[]){int i;for(i=0;i<STR_SIZE;i++){if(b[i]==0) break;} return(i);}
int ccnt_wh(char b[]){int i=0;while(i<STR_SIZE){if(!b[i]) goto ret; i++;} ret: return(i);}
int ccnt_goto(char b[]){int i=0;lbl: if(b[i]!=0 && ++i<STR_SIZE){ goto lbl;}return(i);}
int (*ccnt)(char*) = ccnt_goto;

void error(char* errmsg){ fprintf(stderr,"%s%s",errmsg,NL_SEQ); exit(1); } //err&exit
void println(str) char* str; {if(str!=0)write(1,str,ccnt(str));write(1,NL_SEQ,2);}
/*                                           try write(1,NL_SEQ,sizeof(NLSEQ)) */

int main()
{
int i=0; char c='1'; long l=0; float fl=1; double d=3.3;
short si = 0; unsigned short usi; char* f=sz_lic,*t=sz_buf;
#ifndef __unix__
 if((99)/(int)(d*10) != 3 ) error("line never printed");
 if((fl*9.9)/d != 3 ) error("Error: Not Unix-like system detected!");
#endif
write(1,sz_ident,sizeof(sz_ident)); println(i); println(sz_ident);
usi=--si; if(usi>0) write(1,sz_copy,sizeof(sz_copy)); println(0);
while(i<ccnt_for(f)){t[i]=f[++i];};t[1]=c;write(1,t,ccnt_wh(sz_lic)); println(0);
}
