#include<unistd.h>
#include<stdio.h>
#include <sys/types.h>
#include <signal.h>
char* sz_ident="CUNC day1_fork.c"; /* comment */
char sz_copy[]="(c) EustroSoft.org 2020";
char* sz_lic= "LICENSE: BALES || MIT ||BSD on your choice";
struct child {pid_t pid; char* s; };
#define MAX_CHILDREN 16
#define MAX_WAIT 16
#define SLEEP_TIME 1
struct child ch[MAX_CHILDREN];
pid_t my_pid=0; pid_t new_pid=0; int ch_cnt=-1; // child_count

void sig_handle(int sig) { printf("%s \n",ch[ch_cnt].s); fflush(stdout); }

int main(int argc,char** argv)
{
int i=0; char c='1'; long l=0; ch[0].s=sz_ident;ch[1].s=sz_copy;ch[2].s=sz_lic;
my_pid=getpid();
while(my_pid==getpid())
{
ch_cnt++; if(ch_cnt>=MAX_CHILDREN) break;
if(ch_cnt>3) { ch[ch_cnt].s=sz_ident; if((ch_cnt-3)<argc) ch[ch_cnt].s=argv[ch_cnt-3]; }
new_pid=fork(); ch[ch_cnt].pid=new_pid;
}
if(my_pid==getpid()) // I am parent process
 { while(i<MAX_CHILDREN){sleep(SLEEP_TIME);kill(ch[i].pid,SIGHUP);i++; } }
else { // I am child
     signal(SIGHUP,sig_handle); sig_handle(0); // set signal handler & test it
     while(i++<MAX_WAIT)sleep(SLEEP_TIME);	// wait for MAX_WAIT
}
} //main()

